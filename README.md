# 🐤 FlappyBird clone written in *Lua* with *Löve* 🌙

Simple FlappyBird clone that I write to explore the [LÖVE](https://love2d.org) game engine and the [Lua language](https://lua.org).

<!-- Two images with some space between :3 -->
<p align="center"><img src="docs/demo_menu.png">&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;<img src="docs/demo_gameplay.png"</p>


## Installation

You want to play? Just check the [**releases**](https://codeberg.org/kevinmarquesp/flappy_bird/releases) and pick a version for your platform that you want and play. Enjoy! 😄


## Development


### Files structure

```
├── docs/
│   ├── [...] ⇒ Documentation pages/assets
├── src/
│   ├── assets/
│   │   ├── fonts/
│   │   │   └── [...]
│   │   ├── sprites/
│   │   │   └── [...] ⇒ Colection of png files
│   │   └── appIcon.png
│   ├── lib/
│   │   └── [...] ⇒ Files of Lua libraries
│   ├── obj/
│   │   └── [...] ⇒ All game objects, plain talbes or a table with a :new() method
│   ├── conf.lua
│   ├── globals.lua
│   ├── helpers.lua
│   └── main.lua
├── .gitignore
├── LICENSE
└── README.md
```


### Todos

- [ ] Make the build process automatic
- [ ] Build to Win32bit
- [ ] Fix the game icons in Windows versions
- [ ] Add some music and sound-effects
- [ ] Style the text with a black stroke and a dark shadow
- [ ] Rewrite that structure and put the helper functions inside a separate directory


<br><hr>

> Made with *löve* ♥️ by [*kevinmarquesp*](https://codeberg.org/kevinmarquesp) with *Lua* 🇧🇷
