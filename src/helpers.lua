-- Create/update the file stored in ~/.local/share/FlappyBird/data (in Linux)
local function saveScore()
    local data = { bestScore = BestScore }
    SaveData.save(data, SAVEDATA_NAME)
end


-- Rules to generate the corret medal for each score values
local function getMedal()
    if Score < 5 then
        return EmptyMedal

    elseif Score < BestScore/2 then
        return BronzeMedal

    elseif Score >= BestScore/2 then
        return SilverMedal

    elseif Score > BestScore then
        return GoldMedal
    end
end


-- Check if an element is inside a table
function Find(element, table)
    for _, value in ipairs(table) do
        if value == element then
            return true
        end
    end
    return false
end


-- Insert to the PipesTable a new pair of pipes time to time
function SpawnPipes(dt)
    if Time == 0 then
        table.insert(PipesTable, Pipes:new())
    end
    Time = Time + dt -- Start couting
    if Time >= PIPES_DELAY then
        Time = 0 -- Reset counter | see the RiseGameplay() function
    end
end


-- Just draw all the pipes stored in memory
function RenderPipes()
    for _, pipes in ipairs(PipesTable) do
        pipes:draw()
    end
end


-- Check the colistion with the palyer and the pipes object
function IsColiding(player, pipe)
    local isPlayerAligned = player.top > pipe.top and player.bottom < pipe.bottom
    local isPlayerInside = player.right > pipe.left and player.left < pipe.right

    return not isPlayerAligned and isPlayerInside
end


-- The Medal value will be stored with the medal objects created in main.love.load()
function DrawnCurrentMedal()
    Medal:draw()
end


-- Just return a bool value if the section correspond to the parameter
function CurrentSectionIs(sec)
    return CurrentSec == sec
end


-- Change the current section and setup the game values
function RiseGetReady()
    print ':: GetReady section is loading'

    CurrentSec = SEC_GETREADY
    Player.move = false
    Player:reset()
end


-- Change the current section and setup the game values
function RiseGameplay()
    print ':: Gameplay section is loading'

    CurrentSec = SEC_GAMEPLAY
    Player.move = true
    PipesTable = {}
    Time = 0
    Score = 0
end


-- Change the current section and setup the game values
function RiseGameOver()
    print ':: GameOver section is loading'

    CurrentSec = SEC_GAMEOVER
    Player.move = false
    Medal = getMedal()
    if Score > BestScore then
        BestScore = Score
    end
    saveScore()
end


-- Get the info from the local file, or create one if it doenst exist
function LoadData()
    local statusOk, data = pcall(SaveData.load, SAVEDATA_NAME)
    if statusOk then
        print ':: [INFO] Config file loaded with success!'
        BestScore = data.bestScore
    else
        print ':: [WARNING] Config file missing...'
        BestScre = 0
    end
end
