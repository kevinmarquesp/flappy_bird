-- Löve2d function aliases
GR = love.graphics
WN = love.window
EV = love.event
MT = love.math
FS = love.filesystem

-- Enum to handle each section state
SEC_GETREADY = 0
SEC_GAMEPLAY = 1
SEC_GAMEOVER = 2

-- External libraries
Anime8   = require 'lib.anime8'
SaveData = require 'lib.saveData'

-- Game variables that handle the behavior
PipesTable = {}
CurrentSec = SEC_GETREADY
Time       = 0
Score      = 0
BestScore  = Score
Medal      = nil -- check the helpers.GetMedal() and love.draw() in GAME OVER section

-- Game configuration
GAME_WIDTH    = 320
GAME_HEIGH    = 480
PLAYER_MARGIN = 4
PIPES_DELAY   = 3
SAVEDATA_NAME = 'data'

-- Path to all sprites and assets
ASSETS = {
    background   = 'assets/sprites/background.png',
    bronzeMedal  = 'assets/sprites/bronzeMedal.png',
    emptyMedal   = 'assets/sprites/emptyMedal.png',
    floor        = 'assets/sprites/floor.png',
    gameOver     = 'assets/sprites/gameOver.png',
    getReady     = 'assets/sprites/getReady.png',
    goldMedal    = 'assets/sprites/goldMedal.png',
    pipe         = 'assets/sprites/pipe.png',
    playerFrames = 'assets/sprites/playerFrames.png',
    silverMedal  = 'assets/sprites/silverMedal.png'
}
