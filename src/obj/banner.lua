local bannerTable = { y = 50 }

function bannerTable:draw()
    GR.draw(self.image, self.x, self.y, nil, 1)
end

-- Constructor of an object, to each instance have different image paths
function bannerTable:new(imgPath)
    local bannerTableObject = {}
    setmetatable(bannerTableObject, self)

    self.__index = self
    bannerTableObject.image = GR.newImage(imgPath)
    bannerTableObject.x = GAME_WIDTH/2 - bannerTableObject.image:getWidth()/2

    return bannerTableObject
end

return bannerTable
