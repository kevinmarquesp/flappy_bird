--[[
    NOTE: The Y value is randonly generated in :new() method!
]]--

local pipesTable = {
    gap    = 110,
    xSpeed = 1
}

pipesTable.image = GR.newImage(ASSETS.pipe)
pipesTable.w     = pipesTable.image:getWidth()
pipesTable.h     = pipesTable.image:getHeight()
pipesTable.x     = GAME_WIDTH + pipesTable.w


function pipesTable:update()
    self.x = self.x -self.xSpeed
end


function pipesTable:draw()
    GR.draw(self.image, self.x, self.y - self.gap, nil, 1, -1) -- Pipe top (clone)
    GR.draw(self.image, self.x, self.y, nil, 1)                -- Pipe bottom
end


function pipesTable:getHitbox()
    return {
        left   = self.x,
        right  = self.x + self.w,
        bottom = self.y,           -- Thats the top of the BOTTOM pipe!
        top    = self.y - self.gap -- And thats the bottom of the TOP pipe!
    }
end


-- Constructor of an object, to each instance have different properties
function pipesTable:new()
    local pipesTableObject = {}
    setmetatable(pipesTableObject, self)

    self.__index = self
    pipesTableObject.y = MT.random(140, 340)

    return pipesTableObject
end


return pipesTable
