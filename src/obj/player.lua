--[[
    ISSUE: I can't set the canvas sprite width and height ;-;
    NOTE: The margin in :getHitbox() make the hitbox smaller (or bigger, if it is a negative value...)
]]--

local playerTable = {
    x = 25,
    y = 50,
    w = 34,
    h = 24,
    gravityConst = .3,
    jumpConst = 6
}

playerTable.image = GR.newImage(ASSETS.playerFrames)
playerTable.grid  = Anime8.newGrid(playerTable.w, playerTable.h, playerTable.image:getWidth(), playerTable.image:getHeight())
playerTable.anm   = Anime8.newAnimation(playerTable.grid('1-4', 1), .2)

playerTable.move   = false
playerTable.isDead = false
playerTable.ySpeed = 0


function playerTable:update(dt)
    self.anm:update(dt)
    self:_checkCornerColision()

    if self.move then
        self.ySpeed = self.ySpeed + self.gravityConst
        self.y = self.y + self.ySpeed
    end
end


function playerTable:draw()
    self.anm:draw(self.image, self.x, self.y, nil, 1)
end


function playerTable:jump()
    self.ySpeed = -self.jumpConst
end


function playerTable:reset()
    self.y = 50
    self.ySpeed = 0
    self.isDead = false
end


function playerTable:getHitbox(margin)
    return {
        left   = self.x          + margin,
        right  = self.x + self.w - margin,
        bottom = self.y + self.h - margin,
        top    = self.y          + margin
    }
end


-- Kill the player if he is outside of canvas or hit the floor
function playerTable:_checkCornerColision()
    local isOutsideTop    = self.y <= 0
    local isOutsideBottom = self.y + self.h >= GAME_HEIGH - Floor.h

    if isOutsideTop or isOutsideBottom then
        playerTable.isDead = true
    else
        playerTable.isDead = false
    end
end


return playerTable
