--[[
    NOTE: The sprite was duplicated 2.5 times to fit the screen without any gap
]]--

local paralexTable = { x = 0 }
local SPRITE_SLICE_CONST = 2.5


function paralexTable:update()
    self.x = self.x - self.speed

    if self.x <= -1 * (self.w/SPRITE_SLICE_CONST) then
        self.x = 0
    end
end


function paralexTable:draw()
    GR.draw(self.image, self.x, self.y, nil, 1)
end


function paralexTable:new(imagePath, speed)
    local paralexTableObject = {}
    setmetatable(paralexTableObject, self)

    self.__index = self
    paralexTableObject.image = GR.newImage(imagePath)
    paralexTableObject.w     = paralexTableObject.image:getWidth()
    paralexTableObject.h     = paralexTableObject.image:getHeight()
    paralexTableObject.y     = GAME_HEIGH - paralexTableObject.h
    paralexTableObject.speed = speed

    return paralexTableObject
end


return paralexTable
