local medalTable = {
    x = 73,
    y = 137
}


function medalTable:draw()
    GR.draw(self.image, self.x, self.y, nil, 1)
end


function medalTable:new(imagePath)
    local medalTableObject = {}
    setmetatable(medalTableObject, self)

    self.__index = self
    medalTableObject.image = GR.newImage(imagePath)

    return medalTableObject
end


return medalTable
