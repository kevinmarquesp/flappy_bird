require 'globals'
require 'helpers'


function love.load()
    GR.setFont(GR.newFont('assets/fonts/PressStart2P-Regular.ttf', 15))
    WN.setMode(GAME_WIDTH, GAME_HEIGH)
    GR.setDefaultFilter('nearest', 'nearest')
    GR.setBackgroundColor(88/255, 230/255, 252/255)

    Background  = require 'obj.paralex':new(ASSETS.background, .35)
    Floor       = require 'obj.paralex':new(ASSETS.floor, 2)
    Player      = require 'obj.player'
    Pipes       = require 'obj.pipes'
    GetReady    = require 'obj.banner':new(ASSETS.getReady)
    GameOver    = require 'obj.banner':new(ASSETS.gameOver)
    EmptyMedal  = require 'obj.medal':new(ASSETS.emptyMedal)
    BronzeMedal = require 'obj.medal':new(ASSETS.bronzeMedal)
    SilverMedal = require 'obj.medal':new(ASSETS.silverMedal)
    GoldMedal   = require 'obj.medal':new(ASSETS.goldMedal)

    LoadData()
end


function love.update(dt)
    --------- Code block to update game object values in [GET READY] section ---------
    if CurrentSectionIs(SEC_GETREADY) then
        Background:update()
        Player:update(dt)
        Floor:update()

    --------- Code block to update game object values in [GAME PLAY] section ---------
    elseif CurrentSectionIs(SEC_GAMEPLAY) then
        Background:update()
        Player:update(dt)
        Floor:update()

        SpawnPipes(dt)

        for key, pipes in ipairs(PipesTable) do
            local playerHitbox = Player:getHitbox(PLAYER_MARGIN)
            local pipesHitbox = pipes:getHitbox()
            pipes:update()

            if pipes:getHitbox().right <= 0 then
                table.remove(PipesTable, key) -- Delete the pipe from the table
                Score = Score + 1

            elseif IsColiding(playerHitbox, pipesHitbox) then
                Player.isDead = true
            end
        end

        if Player.isDead then
            RiseGameOver()
        end

    --------- Code block to update game object values in [GAME OVER] section ---------
    elseif CurrentSectionIs(SEC_GAMEOVER) then
        Background:update()
    end
end


function love.draw()
    local txtPos = {                                             -- Table to store the text position from GAME OVER section
        x          = GAME_WIDTH/2 - GameOver.image:getWidth()/2, -- Middle of the screen
        scoreY     = 130,                                        -- Upper label position
        bestScoreY = 173,                                        -- Bottom label position
        limit      = GameOver.image:getWidth() - 15              -- Make it align to rigth
    }

    --------- Code block to draw into the [GET READY] section ---------
    if CurrentSectionIs(SEC_GETREADY) then
        Background:draw()
        Player:draw()
        GetReady:draw()
        Floor:draw()

    --------- Code block to draw into the [GAME PLAY] section ---------
    elseif CurrentSectionIs(SEC_GAMEPLAY) then
        Background:draw()
        Player:draw()
        RenderPipes() -- pipes:draw()
        Floor:draw()
        GR.print(Score, 10, GAME_HEIGH - 25)

    --------- Code block to draw into the [GAME OVER] section ---------
    elseif CurrentSectionIs(SEC_GAMEOVER) then
        Background:draw()
        Player:draw()
        RenderPipes()
        GameOver:draw()
        DrawnCurrentMedal()
        GR.printf(Score, txtPos.x, txtPos.scoreY, txtPos.limit, 'right')
        GR.printf(BestScore, txtPos.x, txtPos.bestScoreY, txtPos.limit, 'right')
        Floor:draw()
    end
end


function love.keypressed(key)
    if Find(key, { 'escape', 'backspace', 'x', 'q' }) then
        EV.quit()
    end

    if Find(key, { 'space', 'return', 'w', 'k' }) then
        if CurrentSectionIs(SEC_GETREADY) then
            RiseGameplay()

        elseif CurrentSectionIs(SEC_GAMEPLAY) then
            Player:jump()

        elseif CurrentSectionIs(SEC_GAMEOVER) then
            RiseGetReady()
        end
    end
end
