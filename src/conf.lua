function love.conf(t)
    t.identity    = 'FlappyBird'
    t.version     = '11.4'
    t.window.icon = 'assets/appIcon.png'
    t.url         = 'codeberg.org/kevinmarquesp/flappy_bird'

    t.window.title      = 'Flappy Bird - Clone'
    t.window.width      = 320
    t.window.height     = 480
    t.window.resizable  = false
    t.window.fullscreen = false
    t.window.display    = 1
end
